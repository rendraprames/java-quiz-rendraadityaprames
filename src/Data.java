import java.util.HashMap;

public class Data {
    private HashMap<Integer, Integer> voucher = new HashMap<>();
    public void setVoucher(HashMap<Integer, Integer> voucher) {
        this.voucher = voucher;}

    public HashMap<Integer, Integer> getVoucher() {
        return voucher;
    }

    public void listVoucher() {
        HashMap<Integer, Integer> vouchers = new HashMap<>();
        vouchers.put(10000, 100);
        vouchers.put(25000, 200);
        vouchers.put(50000, 400);
        vouchers.put(100000, 800);
        setVoucher(vouchers);
    }
}
