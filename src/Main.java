import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        // membuat scanner baru
        Scanner input = new Scanner(System.in);
        PointService pointService = new PointService();
        Integer pilih = null;

        /**
         * Loop Menu
         */
        do {
            mainMenu();
            pilih = input.nextInt();
            switch (pilih) {
                case 1:
                    pointService.biggestVoucher();
                    pressEnter();
                    break;
                case 2: ;
                    pointService.getPointLeftAfterRedeem(1000);
                    pressEnter();
                    break;
                case 3: ;
                    Scanner scaner = new Scanner(System.in);
                    System.out.print("| Masukkan point anda = ");
                    int point = scaner.nextInt();
                    System.out.println("| Point anda adalah " + point);
                    System.out.println("| Voucher yang anda dapatkan adalah " + pointService.allVoucherPoints(point));
                    System.out.println("| Sisa point anda adalah " + pointService.pointLeft(point) + " point");
                    pressEnter();
                    break;
                case 4:
                    System.out.println("+-------------------------------------+");
                    System.out.println("|            System Exit              |");
                    System.out.println("+-------------------------------------+");
                    break;
                default:
                    System.out.println("| Pilihan anda tidak ada");
            }
        } while (pilih != 4);

    }

    /**
     * Menu Interface Method
     */
    public static void mainMenu(){
        System.out.println("+-------------------------------------+");
        System.out.println("|            Voucher Redeem           |");
        System.out.println("+-------------------------------------+");
        System.out.println("| Voucher 10rb = 100p                 |");
        System.out.println("| Voucher 25rb = 200p                 |");
        System.out.println("| Voucher 50rb = 400p                 |");
        System.out.println("| Voucher 100rb = 800p                |");
        System.out.println("+-------------------------------------+");
        System.out.println("| 1. Tukar Points Ke Voucher Terbesar |");
        System.out.println("| 2. Tukar 1000 Points                |");
        System.out.println("| 3. Tukar Semua Points               |");
        System.out.println("| 4. Exit Program                     |");
        System.out.println("+-------------------------------------+");
        System.out.print("| Pilihanmu = ");
    }

    /**
     * Press Enter Back To Menu Method
     */
    public static void pressEnter () {
        System.out.println("| Press Enter To Continue...");
        try {
            System.in.read();
        } catch (Exception e) {}
    }
}
