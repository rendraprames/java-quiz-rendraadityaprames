import java.util.*;

public class PointService {
    Data voucherData = new Data();

    /*
     * Kuis Nomor 1
     */
    public void biggestVoucher(){
        voucherData.listVoucher();
        var vouchers = voucherData.getVoucher();
        Integer max = Collections.max(vouchers.values());
        for (Map.Entry<Integer, Integer> entry : vouchers.entrySet()){
            if (entry.getValue() == max){
                System.out.println("| Voucher pulsa terbesar adalah Rp. " + entry.getKey() + " dapat ditukar dengan " + entry.getValue() + " point");
            }
        }
    }
    /*
     * Kuis Nomor 2
     */
    public Integer getPointLeftAfterRedeem(int point){
        voucherData.listVoucher();
        var voucherList = voucherData.getVoucher();
        Integer max = Collections.max(voucherList.values());
        Integer pointLeft = point - max;
        System.out.println("| 1000 Point Telah Ditukarkan");
        Integer voucherTerbesar = Collections.max(voucherList.values());
        for (Map.Entry<Integer, Integer> entry : voucherList.entrySet()){
            if (entry.getValue() == voucherTerbesar){
                System.out.println("| Anda Mendapatkan Voucher Sebesar Rp. " + entry.getKey() + " Dengan Total Point " + entry.getValue() + "p");
            }
        }
        System.out.println("| Sisa Point Anda Adalah " + pointLeft + "p");
        return pointLeft;
    }

    /*
     * Kuis Nomor 3
     * Sorting Voucher
     */
    public List<Integer> sortingVoucherData(){
        var list = voucherData.getVoucher();
        List<Integer> sorting = new ArrayList<>(list.values());
        sorting.sort(Collections.reverseOrder());
        return sorting;
    }

    /**
     * Method Menemukan Voucher yang didapat dari point yang dimiliki, biggest voucher first
     * @param point
     * @return
     */
    public List<Integer> allVoucherPoints(int point){
        voucherData.listVoucher();
        var list = voucherData.getVoucher();
        List<Integer> points = sortingVoucherData();
        List<Integer> redeem = new ArrayList<>();
        List<Integer> voucher = new ArrayList<>();
        for (Integer value : points){
            if (point >= value){
                point = point - value;
                redeem.add(value);
            }
        }
        for (Integer i : redeem) {
            for (Map.Entry<Integer, Integer> entry : list.entrySet()){
                if (Objects.equals(entry.getValue(), i)){
                    voucher.add(entry.getKey());
                }
            }
        }return voucher;
    }

    /**
     * Sisa Point Seteleh Ditukar
     * @param point
     * @return
     */
    public Integer pointLeft(int point) {
        List<Integer> values = sortingVoucherData();
        for (Integer value : values) {
            if (point >= value) {
                point = point - value;
            }
        }return point;
    }
}
